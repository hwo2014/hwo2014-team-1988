import json
import socket
import sys

class Piece(object):

    def __init__(length, radius, angle, switch):
        self.length = length
        self.radius = radius
        self.angle = angle
        self.switch = switch

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.pieces = []
        self.currentIndex = 0;

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchLane(self, direction):
        #print("Switching " + direction)
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        currentIndex = data[0]['piecePosition']['pieceIndex']
        nextIndex = currentIndex + 2
        nextIndex = nextIndex % len(self.pieces)

        if self.currentIndex != currentIndex:
            print("Piece #" + str(currentIndex))
            self.currentIndex = currentIndex

        if u'angle' in self.pieces[nextIndex]: # is next piece curve
            if (self.pieces[nextIndex]['angle'] > 0): # left or right curve
                self.switchLane("Right")
            else:
                self.switchLane("Left")

        self.throttle(0.6)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_gameinit(self, data):
        pieceList = data['race']['track']['pieces']
        for piece in pieceList:
            self.pieces.append(piece)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_gameinit,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
